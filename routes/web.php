<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/register', 'AuthController@reg');
Route::post('/welcome', 'AuthController@wel');

Route::get('/data-table', 'IndexController@table');
//Route::get('/master', function(){
//    return view('layout.master');
//});
