<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function reg()
    {
        return view('page.register');
    }
    
    public function wel(Request $request)
    {
        $first = $request['first'];
        $last = $request['last'];
        return view('page.welcome', compact("first", 'last'));
    }
}
