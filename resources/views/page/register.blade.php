@extends('layout.master')
@section('title')
Buat Account Baru
@endsection

@section('subtitle')
Sign UP Form
@endsection

@section('content')
<form action="/welcome" method="POST">
    @csrf
    <label>First Name :</label><br>
    <input type="text" name="first"><br><br>
    <label>Last Name :</label><br>
    <input type="text" name="last"><br><br>
    <label>Gender</label><br>
    <input type="radio">Male<br>
    <input type="radio">Female<br><br>
    <label>Language Spoken</label><br>
    <input type="checkbox">Indonesia<br>
    <input type="checkbox">English<br>
    <input type="checkbox">Other<br><br>
    <label>Nationalty</label><br>
    <input list="Kebangsaan">
    <datalist id="Kebangsaan">
        <option value="Indonesia">
        <option value="Warga Negara Asing">
    </datalist><br><br>
    <textarea rows="4" cols="50">
        
        </textarea>
        <br><br>
        <input type="submit" value="Sign Up"> 
</form>   

@endsection
    <h2></h2>
    <h4>Sign Up From</h4>
